package slickbase;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.Graphics;

import slickbase.GameManager;
import slickbase.entities.Entity;


public class GameManagerTest {

	private GameManager gmt;
	private MockEntity e;
	
	@Before
	public void setup(){
		gmt = new GameManager();
		e = new MockEntity();
		gmt.registerEntity(e);
	}
	
	@Test
	public void testAddEntity() {		
		assertEquals(1, gmt.getEntityCount());
	}
	
	@Test
	public void testUpdateAndRenderEntity() {		
		gmt.update(5);
		assertEquals(5, e.storedDelta);
		gmt.render(null);
		assertEquals(10, e.storedDelta);		
	}	
}

class MockEntity extends Entity {

	public int storedDelta = 0;
	@Override
	public void update(int delta) {
		storedDelta = delta;		
	}

	@Override
	public void render(Graphics g) {
		storedDelta = storedDelta * 2;		
	}
	
}
