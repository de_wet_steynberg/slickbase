package slickbase;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;

import slickbase.entities.Entity;

public class GameManager extends Entity {

	private List<Entity> entities = new ArrayList<Entity>();
	
	@Override
	public void update(int delta) {
		for (Entity entity : entities) {
			entity.update(delta);
		}		
	}

	@Override
	public void render(Graphics g) {
		for (Entity entity : entities) {
			entity.render(g);
		}		
	}
	
	public void registerEntity(Entity entity)
	{
		entities.add(entity);
	}	
	
	public long getEntityCount() {
		return entities.size();
	}

}
