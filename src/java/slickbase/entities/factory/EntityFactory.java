package slickbase.entities.factory;

import org.newdawn.slick.SlickException;

import slickbase.entities.Entity;


public interface EntityFactory {
	public Entity createEntity() throws SlickException;
}
