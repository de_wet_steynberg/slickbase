package slickbase.entities;

import org.newdawn.slick.Graphics;

public abstract class Entity {
	public abstract void update(int delta);
	public abstract void render(Graphics g);
	
}
