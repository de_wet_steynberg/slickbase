package slickbase.component;

import org.newdawn.slick.Graphics;

import slickbase.entities.Entity;


public interface RenderableComponent extends Component {
	public abstract void render(Graphics g, Entity entity);
}
